package interfaces;

public interface Jumpable {
    double jump();
}
