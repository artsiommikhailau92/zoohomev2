package animals;

final public class DomesticCat extends Filine {
    private String breed;
    final private String sound = "Meay";


    public DomesticCat(String nickName, double size) {
        super(nickName, size);
        setType("Cat");
    }

    @Override
    public void sound() {
        System.out.println(sound);
    }

    @Override
    public double jump() {
        return getSize() * 2;
    }
}
