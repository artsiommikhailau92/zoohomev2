package animals;

final public class ForestWolf extends Canine {
    public ForestWolf(String nickName, double size) {
        super(nickName, size);
        setType("Wolf");


    }

    @Override
    public void sound() {
        System.out.println("Rrrr ");
    }

    @Override
    public double jump() {
        return getSize() * 1.5;
    }

}
