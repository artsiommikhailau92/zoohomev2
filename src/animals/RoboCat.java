package animals;

final public class RoboCat extends Filine {
    static final double DEFAULT_SIZE = 5;
    static int objectCounter = 0;
   public VoiceModule voiceModule = new VoiceModule();


    public RoboCat() {
        super("Robocat #" + objectCounter, DEFAULT_SIZE);
        objectCounter++;

    }

    @Override
    public void sound() {
        System.out.println(voiceModule.getSound());
    }

    public static class VoiceModule {
        String sound = "Kill all people!";

        public VoiceModule() {
        }

        public VoiceModule(String sound) {
            this.sound = sound;
        }

        public String getSound() {
            return sound;
        }

        public void setSound(String sound) {
            this.sound = sound;
        }

    }

}
