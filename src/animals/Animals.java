package animals;

import interfaces.Jumpable;
import interfaces.Soundable;

abstract public class Animals implements Soundable, Jumpable {
    private String nickName;
    private double size;
    private String type;

    public Animals(String nickName, double size) {
        this.nickName = nickName;
        this.size = size;
    }

    @Override
    public String toString() {
        return ("Animal " + this.type + ", name " + this.nickName + ", size " + this.size);
    }


    @Override
    public void sound() {
    }

    @Override
    public double jump() {
        return 0;
    }

    public String getNickName() {
        return nickName;
    }

    public double getSize() {
        return size;
    }

    public String getType() {
        return type;
    }

    final public void setType(String type) {
        this.type = type;
    }


}
