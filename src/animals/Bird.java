package animals;

final public class Bird extends Animals {
    public Bird(String nickName, double size) {
        super(nickName, size);
    }

    @Override
    public void sound() {
        System.out.println("Piy piy");
    }

    @Override
    public double jump() {
        return getSize() * 4;
    }
}
