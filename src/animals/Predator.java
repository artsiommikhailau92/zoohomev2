package animals;

abstract public class Predator extends Mammal {
    private boolean isScavenger;

    public Predator(String nickName, double size) {
        super(nickName, size);
    }
}
