package animals;

abstract public class Canine extends Predator {
    public Canine(String nickName, double size) {
        super(nickName, size);
    }
}
