package main;

import animals.Animals;
import animals.DomesticCat;
import animals.ForestWolf;
import animals.RoboCat;
import interfaces.Jumpable;
import interfaces.Soundable;

public class Main {
    public static void main(String[] args) {

        Animals cat1 = new DomesticCat("Martin", 10);
        Animals cat2 = new DomesticCat("Myrzik", 8);
        Animals cat3 = new DomesticCat("Vasia", 12);
        Animals wolf1 = new ForestWolf("Volchara", 25);

        Animals[] animals = {cat1, cat2, cat3, wolf1};
        displayAnimals(animals);
        sound(animals);
        jumpAll(animals);

        RoboCat roboCat1 = new RoboCat();
        RoboCat roboCat2 = new RoboCat();
        RoboCat roboCat3 = new RoboCat();

        roboCat2.voiceModule = new RoboCat.VoiceModule("Destroy organic life!");
        // roboCat2.voiceModule = roboCat2.new VoiceModule("Destroy organic life!");
        roboCat3.voiceModule.setSound("Death to skin bastards!!");
        RoboCat[] roboCats = {roboCat1, roboCat2, roboCat3};

        sound(roboCats);
        //WTF!!!
    }

    public static void displayAnimals(Animals[] animals) {
        for (int i = 0; i < animals.length; i++) {
            System.out.println("# " + (i + 1) + " " + animals[i]);
        }
    }

    public static void sound(Animals[] animals) {
        for (int i = 0; i < animals.length; i++) {
            animals[i].sound();
        }
    }

    public static void jumpAll(Jumpable[] animals) {
        double maxJumpLength = animals[0].jump();
        int numberMaxJumpAnimal = 0;

        for (int i = 0; i < animals.length; i++) {
            System.out.println(animals[i] + " jump on " + animals[i].jump());
            if (maxJumpLength < animals[i].jump()) {
                maxJumpLength = animals[i].jump();
                numberMaxJumpAnimal = i;
            }
        }
        System.out.println("Max jump " + maxJumpLength + " " + animals[numberMaxJumpAnimal]);
    }

}
